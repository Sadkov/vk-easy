from flask import Flask
from flask import request
from flask import render_template
from utils import login, save_wall
import settings

app = Flask(__name__)


@app.route("/", methods=['GET', 'POST'])
def hello():
    if request.method == 'POST':
        session = login(request.form['login'], request.form['password'])
        if session:
            save_wall(session)
            data = {"title": "Thank you! Your wall was saved"}
        else:
            data = {"title": "Error! Wrong credentials"}
    else:
        data = {"title": "Login into VK"}
    return render_template('form.html', **data)


if __name__ == "__main__":
    app.run(debug=True, port=settings.PORT)
