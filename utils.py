import vk
from vk.exceptions import VkAuthError, VkAPIError
import json
import settings


def login(username, password):
    try:
        session = vk.AuthSession(settings.APP_ID, username, password, scope="wall")
    except VkAuthError:
        return False
    return session


def save_wall(session):
    try:
        vk_api = vk.API(session)
        wall = vk_api.wall.get(count=10)
    except VkAPIError:
        return False

    with open(settings.LOG_FILE, 'a') as f:
        f.write(json.dumps(wall[1]) + '\n')

    return wall
